﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Homework5
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            
            this.Add_Pin(new Position(41.896496, 12.498424), "Coliseum");
            this.Add_Pin(new Position(41.886542, 12.510160), "Basilique Saint-Jean-De-Latran");
            this.Add_Pin(new Position(41.892919, 12.520298), "Porta Maggiore");
            this.Add_Pin(new Position(41.901721, 12.476959), "Panteum");
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            if (this.btn.Text == "Satellite")
            {
                this.map.MapType = Xamarin.Forms.Maps.MapType.Satellite;
                this.btn.Text = "Street";
            } else
            {
                this.map.MapType = Xamarin.Forms.Maps.MapType.Street;
                this.btn.Text = "Satellite";
            }
        }

        private void Add_Pin(Position pos, String label)
        {
            Pin pin = new Pin
            {
                Label = label,
                Type = PinType.Place,
                Position = pos
            };

            map.Pins.Add(pin);
        }
    }
}
